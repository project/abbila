<?php
// $Id$

/**
 * @file
 *   Abbila Lib file.
 */


/**
 * sends request to Abbila Search Engine
 *
 * @param object $what
 * @param object $query [optional]
 * @return
 */
function _abbila_execute_request($what, $query = NULL, $type = NULL) {
  $ch = curl_init();

  $abbila_address = variable_get('abbila_host', NULL);

  switch ($what) {
    case 'index':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      $file = file_directory_path() . '/abbila_index.xml';
      $h = fopen($file, 'r');
      curl_setopt($ch, CURLOPT_UPLOAD, 1);
      curl_setopt($ch, CURLOPT_INFILE, $h);
      curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file));
      break;

    case 'delete':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      break;

    case 'single_delete':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents/' . $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      break;

    case 'retrieve':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents/search?detail=2&query=extID:' . $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      break;

    case 'search':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents/search?detail=2&count=50&query=' . $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      break;

    case 'similar':
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/documents/search?detail=2&count=' . variable_get('abbila_more_to_show', 6) . '&query=channel:' . $type . '&query=' . $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      break;

    case 'check':
      curl_setopt($ch, CURLOPT_URL, $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      break;

    case 'onthology_list':
      $abbila_address = drupal_substr($abbila_address, 0, strrpos($abbila_address, '/'));
      curl_setopt($ch, CURLOPT_URL, $abbila_address);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      break;

    case 'onthology_delete':
      $abbila_address = drupal_substr($abbila_address, 0, strrpos($abbila_address, '/'));
      curl_setopt($ch, CURLOPT_URL, $abbila_address . '/' . $query);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      break;

    case 'onthology_load':
      $abbila_address = drupal_substr($abbila_address, 0, strrpos($abbila_address, '/'));
      curl_setopt($ch, CURLOPT_URL, $abbila_address);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      $file = file_directory_path() . '/abbila/onthology.owl';
      $h = fopen($file, 'r');
      curl_setopt($ch, CURLOPT_UPLOAD, 1);
      curl_setopt($ch, CURLOPT_INFILE, $h);
      curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file));
      break;
  }

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $result = curl_exec($ch);
  curl_close($ch);
  if ($what == 'index') {
    fclose($h);
  }
  return $result;
}



/**
 * Maps the arrays and returns the body
 *
 * @return
 */
function _abbila_node_walker($cck, $key, $body) {
  if ($cck['value']) {
    $body .= $cck['value'] . '<br/>';
  }
}