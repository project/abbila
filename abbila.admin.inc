<?php
// $Id$

/**
 * @file
 *   Administrative pages for the Abbila Search Engine module.
 */


/**
 * Host Configuration Form
 *
 * @param object $form_state
 * @return
 */
function abbila_config_form($form_state) {
  $options = array(
    5 => 5,
    10 => 10,
    15 => 15,
    20 => 20,
    25 => 25,
    30 => 30,
    50 => 50,
    100 => 100,
    150 => 150,
    200 => 200
  );
  $options_more = array(
    2 => 1,
    3 => 2,
    4 => 3,
    5 => 4,
    6 => 5,
    7 => 6,
    8 => 7,
    9 => 8,
    10 => 9,
    11 => 10
  );

  $result = db_query("SELECT type from {node_type}");
  while ($row = $result->fetch_object()) {
    $node_types[$row->type] = $row->type;
  }

  $form['abbila_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Abbila Host Address'),
    '#default_value' => variable_get('abbila_host', NULL),
    '#size' => '100',
    '#maxlength' => '240',
    '#required' => TRUE,
    '#description' => 'ie: http://[root_abbila]/[sandbox]/domains/[domain_number]'
  );
  $form['abbila_nodes_to_index'] = array(
    '#type' => 'select',
    '#title' => t('Number of Nodes to Index'),
    '#default_value' => variable_get('abbila_nodes_to_index', 5),
    '#size' => '1',
    '#options' => $options,
    '#description' => 'number of nodes indexed after every cron'
  );
  $form['abbila_more_to_show'] = array(
    '#type' => 'select',
    '#title' => t('Results of More of the Same'),
    '#default_value' => variable_get('abbila_more_to_show', 6),
    '#size' => '1',
    '#options' => $options_more,
    '#description' => 'number of results to display for more of the same'
  );
  $form['abbila_nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types'),
    '#collapsible' => true,
    '#collapsed' => true
  );
  $form['abbila_nodes']['abbila_node_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('abbila_node_type', array()),
    '#options' => $node_types,
    '#description' => t('Select the Node Types you want to be indexed.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}



/**
 * Index Delete Form
 *
 * @param object $form_state
 * @return
 */
function abbila_delete_index_form($form_state) {
  $form['submit'] = array('#type' => 'submit', '#value' => t('Delete Index'));
  return $form;
}



/**
 * CCK Configuration Form
 *
 * @param object $form_state
 * @return
 */
function abbila_cck_config_form($form_state) {
  $checks = array();
  $result = db_query("SELECT DISTINCT field_name as cck from {content_node_field_instance}");
  while ($row = $result->fetch_object()) {
    $checks[$row->cck] = $row->cck;
  }


  $form['ccks'] = array(
    '#type' => 'checkboxes',
    '#title' => t('CCK Fields'),
    '#default_value' => variable_get('abbila_ccks', array()),
    '#options' => $checks,
    '#description' => t('Select the CCK fields you want to be indexed.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}


/**
 * Onthology Settings Form
 *
 * @param object $form_state
 * @return
 */
function abbila_onthology_config_form($form_state) {
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['abbila_onthology'] = array(
    '#type' => 'file',
    '#title' => t('File .owl used to load the Onthology'),
    '#size' => '40'
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Load the new Onthology'));
  return $form;
}


/**
 * Onthology Delete Form
 *
 * @param object $form_state
 * @return
 */
function abbila_onthology_delete_form($form_state) {
  $form['submit'] = array('#type' => 'submit', '#value' => t('Delete Abbila Onthologies'));
  return $form;
}


/**
 * Host Form Validate
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_config_form_validate($form, &$form_state) {
  $host = $form_state['values']['abbila_host'];
  $result = _abbila_execute_request('check', $host);
  if (strpos($result, '<description>Dominio') === FALSE) {
    form_set_error('abbila_host', t('There is not an Abbila Sandbox at the specified Address.'));
    watchdog('Abbila', 'Impossible to find Abbila Sandbox: @host', array('@host' => $host), WATCHDOG_ERROR );
  }
}


/**
 * Host Form Submit
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_config_form_submit($form, &$form_state) {
  variable_set('abbila_host', $form_state['values']['abbila_host']);
  variable_set('abbila_nodes_to_index', $form_state['values']['abbila_nodes_to_index']);
  variable_set('abbila_more_to_show', $form_state['values']['abbila_more_to_show']);
  db_query("UPDATE {abbila} set abbila_host='%s', abbila_nodes_to_index=%d, abbila_more_to_show=%d", $form_state['values']['abbila_host'], $form_state['values']['abbila_nodes_to_index'], $form_state['values']['abbila_more_to_show']);
  db_query("DELETE FROM {abbila_node_type}");
  $selected_node_types = array();
  foreach ($form_state['values']['abbila_node_type'] as $node_type) {
    if ($node_type) {
      db_query("INSERT INTO {abbila_node_type} (type) VALUES ('%s')", $node_type);
      $selected_node_types[] = $node_type;
    }
  }
  variable_set('abbila_node_type', $selected_node_types);
  drupal_set_message(t('The new settings have been saved.'));
  $form_state['redirect'] = 'admin/settings/abbila';
}


/**
 * Index Delete Submit Form
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_delete_index_form_submit($form, &$form_state) {
  $result = _abbila_execute_request('delete');
  if (strpos($result, '200') !== FALSE) {
    db_query("UPDATE {abbila_index} SET indexed = %d", 0);
    watchdog('Abbila', 'The nodes have been deleted from Abbila search engine');
    drupal_set_message(t('The Abbila Index has been deleted.'));
  }
  else {
    watchdog('Abbila', 'It wasn\'t possible to delete the nodes from the Abbila search engine', array(), WATCHDOG_ERROR );
    drupal_set_message(t('The Abbila Index not deleted.'), 'error');
  }
}


/**
 * CCK Form Submit
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_cck_config_form_submit($form, &$form_state) {
  db_query("DELETE FROM {abbila_ccks}");
  $selected_ccks = array();
  foreach ($form_state['values']['ccks'] as $cck) {
    if ($cck) {
      db_query("INSERT INTO {abbila_ccks} (cck) VALUES ('%s')", $cck);
      $selected_ccks[] = $cck;
    }
  }
  variable_set('abbila_ccks', $selected_ccks);
  drupal_set_message(t('The new CCK Abbila settings have been saved.'));
  $form_state['redirect'] = 'admin/settings/abbila/ccks';
}


/**
 * Onthology Form Submit
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_onthology_config_form_submit($form, &$form_state) {

  //define your limits for the submission here
  $limits = array('extensions' => array('owl')) ;

  $validators = array(
    'file_validate_extensions' => array($limits['extensions'])
  );


  // Save new file uploads.
  if ($file = file_save_upload('abbila_onthology', $validators, file_directory_path() . '/')) {
    $extension = drupal_substr($file->filename, drupal_strlen($file->filename) - 3);
      if ($extension == 'owl') {
        file_delete(file_directory_path() . '/onthology.owl');
        file_move($file->filepath, file_directory_path() . '/onthology.owl');
        _abbila_execute_request('onthology_load');
        drupal_set_message(t('The new Onthology has been loaded.'));
        $form_state['redirect'] = 'admin/settings/abbila/';
      }
      else {
        file_delete($file->filepath);
        drupal_set_message(t('The file you tried to use is not .owl.'));
        $form_state['redirect'] = 'admin/settings/abbila/onthology';
      }
  }
  else {
    drupal_set_message(t("It wasn't possible to load the file."));
  }
}



/**
 * Onthology Delete Submit Form
 *
 * @param object $form
 * @param object $form_state
 * @return
 */
function abbila_onthology_delete_form_submit($form, &$form_state) {
  $xml = _abbila_execute_request('onthology_list');
  $dom = new DOMDocument('1.0', 'UTF-8');
  $dom->loadXML($xml);

  $domain = $dom->getElementsByTagName('domain');
  foreach ($domain as $domain) {
    $href = $domain->getAttribute('href');
    $href = drupal_substr($href, strrpos($href, '/') + 1);
    _abbila_execute_request('onthology_delete', $href);
  }
  drupal_set_message(t('The Abbila Onthologies have been deleted.'));
}



/**
 * Page Callback
 * @return
 */
function abbila_config() {
  $form = drupal_get_form('abbila_config_form');
  $form .= drupal_get_form('abbila_delete_index_form');
  return $form;
}


/**
 * Page Callback
 * @return
 */
function abbila_cck_config() {
  $form = drupal_get_form('abbila_cck_config_form');
  return $form;
}


/**
 * Page Callback
 * @return
 */
function abbila_onthology_config() {
  $form = drupal_get_form('abbila_onthology_config_form');
  $form .= drupal_get_form('abbila_onthology_delete_form');
  return $form;
}