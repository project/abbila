## Introduction

This Abbila module let's you implement the Abbila Semantic Search Engine 
created by Hoplo inside your Drupal project.

This module have been created by Nicola Puddu and is mainteined by
Nicola Puddu and Sergio Durzu.

## Description

To use this module you'll need an Abbila sandbox, at the moment 
there are not available test sandbox, but Hoplo is about to provide some.

With this module you will be able to load your own onthologies, 
decide what content types to index inside the sandbox and thanks 
to full CCK support you'll be able to include all or just 
some of the CCK fields inside your content types.

Using Abbila features this module will also allow you to display a block 
inside your page containing links to semantic related contents 
inside your Drupal project.

## Requirements

System requirements:
- Curl
- Php libraries for Curl (if this is not found on the system, a blank page
will be displayed)

Drupal requirements:

- Content Construction Kit
- Search

## Installation

Download the module and extract it to the module's directory and than
enable it.
Go to admin/settings/abbila to configure the module.
You have at least to add an Abbila Host Address.

## To do

- support to node_access
- add blocks of releated content
- translations
- check theming
- bugfixes


